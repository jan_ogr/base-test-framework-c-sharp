﻿![Scheme](jumping-fox.png) 
# Base Test Framework C sharp

Base test framework is a simple starting point for your .net based automated test framework.
The project consists of the sections:

- App: Here you write all logic for the application you are testing.
- Common: The common section contains code that can be shared among all classes. E.g rest client, utils and helpers.
- Models: A section for all class models are stored.
- Test: This section contains your actual tests.

## Coding standards
If you wish to contribute which you are more than welcome to here are a few coding standards to follow.

- Do NOT use constants! Hard coded values are to be configured in the appsettings.json file.
- All code the should be as generic as possible and reusable, do not ad anything project specific.
- Methods are to be WrittenInPascal while private variablesInCamel.
- Use StyleCop and FIX the warnings!

## Usage
All examples are configured to run against http://dummy.restapiexample.com/ which is a dummy api.
Note that the first thing I do is to initialize the Config class (based on appsettings.json). Using the data in this object we build our base url which is 
later on included in the EmployeeApi class. The EmployeeApi class is a wrapper using the RestClient class which contains specific methods for the employee endpoint (just a demo). 
Theoretically new methods for the same endpoint should be added in EmployeeApi. Methods for a new endpoint should NOT be added here, in this case create a new class.

```
    class DemoTests
    {
        private Config conf;
        private EmployeeApi employeeapi;
        private UrlHelpers urlHelpers;

        /// <summary>
        /// One time setup, executed once per test class.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            conf = ConfigHelper.GetConfig();
            employeeapi = new EmployeeApi(conf);
            urlHelpers = new UrlHelpers(conf);
        }

        /// <summary>
        /// One time tear down, executed at the end of every test class.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [OneTimeTearDown]
        public async Task TearDownAsync()
        {
            await employeeapi.CleanUpAsync();
        }
		
        /// <summary>
        /// Test case for fetching employees.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetEmployeesAsync()
        {
            var res = await employeeapi.Get();
            Assert.AreEqual(HttpStatusCode.OK, res.StatusCode);
        }
    }
 
```
## Executing tests
Go to the scr catalogue in the command line and run:
```
dotnet clean
dotnet restore
dotnet build
dotnet test
```

Execute tests and generate an xml report:
```
dotnet test --logger "trx;LogFileName=TestResults.trx"
```

This section will clean the project, restore all dependencies, build and finally run the tests.

## Jenkinsfile
The project contains a basic Jenkinsfile that acts as a starting point for your Jenkins pipeline setup, please install MS Test plug-in for the Publish-test-report step to work. Also the job executes an import script, for this to be executed a number credentials needs to be defined in your Jenkins instance, please see the ImportScript readme file for further instructions.
For more info go to: https://jenkins.io/doc/book/pipeline/



## Contributing
For questions contact jan.ogrodowczyk@sogeti.com
