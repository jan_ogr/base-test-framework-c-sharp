﻿namespace BaseTestFrameworkCSharp.Models.Demo
{
    using Newtonsoft.Json;

    public class GetEmployee
    {
        [JsonProperty("employee_name")]
        public string Name { get; set; }

        [JsonProperty("employee_salary")]
        public string Salary { get; set; }

        [JsonProperty("employee_age")]
        public string Age { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("profile_image")]
        public string Image { get; set; }
    }
}
