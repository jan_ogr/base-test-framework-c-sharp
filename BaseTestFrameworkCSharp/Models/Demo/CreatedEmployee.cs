﻿namespace Base_test_framework_c_sharp.Models.Demo
{
    using Newtonsoft.Json;

    public class CreatedEmployee
    {
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("data")]
        public EmployeeData Data { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }

    public class EmployeeData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("salary")]
        public string Salary { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }
    }
}
