﻿namespace BaseTestFrameworkCSharp.Models.Demo
{
    using Newtonsoft.Json;

    /// <summary>
    /// Demo class models for the dummyapi.
    /// </summary>
    public class CreateEmployee
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("salary")]
        public dynamic Salary { get; set; }

        [JsonProperty("age")]
        public string Age { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
