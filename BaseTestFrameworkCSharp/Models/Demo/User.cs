﻿namespace BaseTestFrameworkCSharp.Models.Demo
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// User dummy class.
    /// </summary>
    public class User
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }
    }
}