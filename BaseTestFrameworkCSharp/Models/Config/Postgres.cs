﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    public class Postgres
    {
        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }
    }
}
