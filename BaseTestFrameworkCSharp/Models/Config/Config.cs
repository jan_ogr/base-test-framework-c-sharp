﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    /// <summary>
    /// Config class model. Based on appsettings.json.
    /// </summary>
    public class Config
    {
        [JsonProperty("rest")]
        public Rest Rest { get; set; }

        [JsonProperty("database")]
        public Database Database { get; set; }

        [JsonProperty("selenium")]
        public Selenium Selenium { get; set; }
    }
}
