﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    public class Rest
    {
        [JsonProperty("http")]
        public string Http { get; set; }

        [JsonProperty("https")]
        public string Https { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("apiPrefix")]
        public string ApiPrefix { get; set; }
    }
}
