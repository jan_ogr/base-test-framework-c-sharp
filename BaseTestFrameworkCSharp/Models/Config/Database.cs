﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    /// <summary>
    /// Appsettings database class.
    /// </summary>
    public class Database
    {
        [JsonProperty("mongodbAtlas")]
        public MongoDbAtlas MongoDbAtlas { get; set; }

        [JsonProperty("postgres")]
        public Postgres Postgres { get; set; }
    }
}
