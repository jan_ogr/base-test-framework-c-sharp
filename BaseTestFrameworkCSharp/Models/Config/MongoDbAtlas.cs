﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    public class MongoDbAtlas
    {
        [JsonProperty("connectionString")]
        public string ConnectionString { get; set; }
    }
}
