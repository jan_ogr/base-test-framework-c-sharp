﻿namespace BaseTestFrameworkCSharp.Models.Config
{
    using Newtonsoft.Json;

    /// <summary>
    /// Selenium class model. Based on appsettings.json.
    /// </summary>
    public class Selenium
    {
        [JsonProperty("screenShots")]
        public string ScreenShots { get; set; }

        [JsonProperty("chromeDriver")]
        public string ChromeDriver { get; set; }

        [JsonProperty("driverUrl")]
        public string DriverUrl { get; set; }
    }
}
