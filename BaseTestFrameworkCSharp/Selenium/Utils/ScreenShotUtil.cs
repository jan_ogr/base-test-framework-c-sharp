﻿namespace BaseTestFrameworkCSharp.Selenium.Utils
{
    using BaseTestFrameworkCSharp.Models.Config;
    using NUnit.Framework;
    using OpenQA.Selenium;

    /// <summary>
    /// Screenshot utility class.
    /// </summary>
    public static class ScreenShotUtil
    {
        /// <summary>
        /// Takes a screensot IF the test result is not successful and saves the .png file under the path specified in config.Selenium.ScreenShots.
        /// </summary>
        /// <param name="imageName">Name of the image.</param>
        /// <param name="config">Config class instance.</param>
        /// <param name="driver">Webdriver instance.</param>
        public static void TakeScreenShot(string imageName, Config config, IWebDriver driver)
        {
            if (TestContext.CurrentContext.Result.Outcome != NUnit.Framework.Interfaces.ResultState.Success)
            {
                Screenshot image = ((ITakesScreenshot)driver).GetScreenshot();
                image.SaveAsFile($"{config.Selenium.ScreenShots}\\{imageName}.png", ScreenshotImageFormat.Png);
            }
        }
    }
}
