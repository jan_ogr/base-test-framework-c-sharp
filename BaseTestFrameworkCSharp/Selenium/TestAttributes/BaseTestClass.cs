﻿namespace BaseTestFrameworkCSharp.Selenium.TestAttributes
{
    using BaseTestFrameworkCSharp.Common.Helpers;
    using BaseTestFrameworkCSharp.Models.Config;
    using BaseTestFrameworkCSharp.Selenium.Utils;
    using NUnit.Framework;
    using OpenQA.Selenium;
    using OpenQA.Selenium.Chrome;

    public abstract class BaseTestClass
    {
        /// <summary>
        /// Base test class for your Selenium test classes.
        /// Inherit from this class and your driver and configfile will be initiated and disposed automatically.
        /// Also screenshots will be taken after each failed test case.
        /// </summary>
        public Config Conf;
        public IWebDriver Driver;

        /// <summary>
        /// Onetime setup method executed once before every test class.
        /// Initializes the Config class.
        /// </summary>
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            Conf = ConfigHelper.GetConfig();
        }

        /// <summary>
        /// SetUp method executed before every tests.
        /// Initializes your webdriver.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            Driver = new ChromeDriver(Conf.Selenium.ChromeDriver)
            {
                Url = Conf.Selenium.DriverUrl,
            };
            Driver.Manage().Window.Maximize();
        }

        /// <summary>
        /// Executed after every test case.
        /// Takes a screenshot if the test fails and shuts down the driver.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
            ScreenShotUtil.TakeScreenShot(TestContext.CurrentContext.Test.Name, Conf, Driver);
            Driver.Close();
            Driver.Quit();
        }
    }
}
