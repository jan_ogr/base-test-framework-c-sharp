﻿namespace BaseTestFrameworkCSharp.Common.Utils.Db
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using BaseTestFrameworkCSharp.Models.Config;
    using Dapper;
    using Npgsql;

    /// <summary>
    /// Postgre SQL utility class. Uses the nugets Npgsql and dapper for easier communication with your postgreSQL database.
    /// </summary>
    public class PostgreUtil
    {
        private readonly NpgsqlConnection connection;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostgreUtil"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="config">Config class inspance.</param>
        public PostgreUtil(Config config)
        {
            connection = new NpgsqlConnection(config.Database.Postgres.ConnectionString);
        }

        /// <summary>
        /// Executes a query agains your postgreSQL database and returns an instance of T.
        /// </summary>
        /// <typeparam name="T">Type T.</typeparam>
        /// <param name="sql">Sql query.</param>
        /// <returns>Instance of T.</returns>
        public IEnumerable<T> Query<T>(string sql)
        {
            var data = new List<T>();
            using (connection)
            {
                connection.Open();
                data.AddRange(connection.Query<T>(sql));
                connection.Close();
            }

            return data;
        }
    }
}
