﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using System;
    using BaseTestFrameworkCSharp.Models.Config;
    using Microsoft.Extensions.Configuration;

    /// <summary>
    /// Configuration helper class.
    /// </summary>
    public static class ConfigHelper
    {
        /// <summary>
        /// Static class for converting the appsetings.json file to a Config class instance.
        /// </summary>
        /// <returns>Config class instance.</returns>
        public static Config GetConfig()
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(AppContext.BaseDirectory)
                    .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            var configuration = builder.Build();
            return new Config()
            {
                Database = configuration.GetSection("database").Get<Database>(),
                Rest = configuration.GetSection("rest").Get<Rest>(),
            };
        }
    }
}
