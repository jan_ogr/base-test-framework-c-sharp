﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using NLog;
    using NUnit.Framework;

    public abstract class BaseTest
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Logger.Info($"\n####### Starting TEST CLASS {TestContext.CurrentContext.Test.ClassName} #######\n");
        }

        [SetUp]
        public void SetUp()
        {
            Logger.Info($"\n####### Starting TEST CASE {TestContext.CurrentContext.Test.Name} #######\n");
        }

        [TearDown]
        public void TearDown()
        {
            Logger.Info($"\n####### Starting TEST CASE {TestContext.CurrentContext.Test.Name} #######\n");
            Logger.Info($"\n####### TEST RESULT: {TestContext.CurrentContext.Result.Outcome}\n");
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Logger.Info($"\n####### Starting TEST CASE {TestContext.CurrentContext.Test.Name} #######\n");
        }
    }
}
