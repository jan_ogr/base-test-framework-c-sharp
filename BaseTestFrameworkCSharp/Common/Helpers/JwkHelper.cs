﻿namespace Base_test_framework_c_sharp.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using Microsoft.IdentityModel.Tokens;
    using Newtonsoft.Json;
    using NLog;
    using Org.BouncyCastle.Crypto.Parameters;

    /// <summary>
    /// Static class for Generating json web keys.
    /// </summary>
    public static class JwkHelper
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Generates a new Json Web key and returns an instance of JsonWebKeySet.
        /// </summary>
        /// <param name="publicKeyPath">Path to your public key.</param>
        /// <returns>An instance of JsonWebKeySet. </returns>
        public static JsonWebKeySet GenerateJwt(string publicKeyPath)
        {
            try
            {
                using var reader = new StringReader(File.ReadAllText(publicKeyPath));
                var keyReader = new Org.BouncyCastle.OpenSsl.PemReader(reader);
                RsaKeyParameters keyParameters = (RsaKeyParameters)keyReader.ReadObject();
                var e = Base64UrlEncoder.Encode(keyParameters.Exponent.ToByteArrayUnsigned());
                var n = Base64UrlEncoder.Encode(keyParameters.Modulus.ToByteArrayUnsigned());
                var dict = new Dictionary<string, string>()
                    {
                        { "e", e },
                        { "kty", "RSA" },
                        { "n", n },
                    };
                var hash = SHA256.Create();
                byte[] hashBytes = hash.ComputeHash(System.Text.Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(dict)));
                JsonWebKey jsonWebKey = new JsonWebKey()
                {
                    Kid = Base64UrlEncoder.Encode(hashBytes),
                    Kty = "RSA",
                    E = e,
                    N = n,
                };
                JsonWebKeySet jsonWebKeySet = new JsonWebKeySet();
                jsonWebKeySet.Keys.Add(jsonWebKey);
                return jsonWebKeySet;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
                throw new Exception(e.Message);
            }
        }
    }
}
