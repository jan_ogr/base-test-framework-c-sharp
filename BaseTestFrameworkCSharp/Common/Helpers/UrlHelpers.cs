﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BaseTestFrameworkCSharp.Models.Config;

    public class UrlHelpers
    {
        /// <summary>
        /// Helper class for building urls or adding query string parameters.
        /// </summary>
        private Config conf;

        public UrlHelpers(Config config)
        {
            conf = config;
        }

        /// <summary>
        /// Adds a set of query string parameters to your base url.
        /// </summary>
        /// <param name="url">base url.</param>
        /// <param name="parameters">List of path strings.</param>
        /// <returns>Uri.</returns>
        public static string AddParameters(string url, Dictionary<string, string> parameters)
        {
            parameters ??= new Dictionary<string, string>();
            return $"{url}?{string.Join("&", parameters.Select(param => $"{param.Key}={param.Value}"))}";
        }

        /// <summary>
        /// Builds a url based on a base url and a list of strings.
        /// </summary>
        /// <param name="ssl">bool.</param>
        /// <param name="pathList">List of path strings.</param>
        /// <returns>Uri.</returns>
        public Uri UrlBuilder(bool ssl = true, List<string> pathList = null)
        {
            pathList ??= new List<string>();
            var url = string.Empty;
            if (ssl)
            {
                url = $"{conf.Rest.Https}{conf.Rest.BaseUrl}/{conf.Rest.ApiPrefix}";
            }
            else
            {
                url = $"{conf.Rest.Http}{conf.Rest.BaseUrl}/{conf.Rest.ApiPrefix}";
            }

            pathList.ForEach(path => url += $"/{path}");
            return new Uri($"{url}/");
        }
    }
}