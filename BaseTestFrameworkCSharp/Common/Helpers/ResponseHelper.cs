﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    using NLog;

    public static class ResponseHelper
    {
        /// <summary>
        /// Static class for converting strings or httpresponsmessages to a T class instance.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Converts HttpResponseMessage. Content to an instance of T.
        /// </summary>
        /// <typeparam name="T">T class.</typeparam>
        /// <param name="responseMessage">HttpResponseMessage.</param>
        /// <returns>Instance of T.</returns>
        public static async Task<T> ToAsync<T>(this HttpResponseMessage responseMessage)
            where T : new()
        {
            var responseData = new T();

            try
            {
                responseData = JsonConvert.DeserializeObject<T>(await responseMessage.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                Logger.Error($"Failed to deserialize object! {e.Message}");
            }

            return responseData;
        }

        /// <summary>
        /// Converts a string to instance of T.
        /// </summary>
        /// <typeparam name="T">T class.</typeparam>
        /// <param name="content">Httpresponse content.</param>
        /// <returns>Instance of T.</returns>
        public static T To<T>(this string content)
            where T : new()
        {
            var responseData = new T();
            try
            {
                responseData = JsonConvert.DeserializeObject<T>(content);
            }
            catch (Exception e)
            {
                Logger.Error($"Failed to deserialize object! {e.Message}");
            }

            return responseData;
        }
    }
}
