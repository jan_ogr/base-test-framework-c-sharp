﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.IO;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using Microsoft.IdentityModel.Tokens;

    /// <summary>
    /// Helper class for generating jwt tokens.
    /// </summary>
    public static class JwtHelpers
    {
        /// <summary>
        /// Static method returning a jwt token.
        /// </summary>
        /// <param name="clientId">Client id, used for aud claim.</param>
        /// <param name="userName">Username used for the sub claim.</param>
        /// <param name="privateKeyPath">path to your private key, used to sign the jwt token.</param>
        /// <param name="kid">Kid, string.</param>
        /// <param name="issuer">issuer, string.</param>
        /// <returns>Returns a jwt token of type string.</returns>
        public static string GenerateJwtToken(string clientId, string userName, string privateKeyPath, string kid = null, string issuer = null)
        {
            // Reading the cert
            string privateKeyPem = File.ReadAllText(privateKeyPath);
            privateKeyPem = privateKeyPem.Replace("-----BEGIN RSA PRIVATE KEY-----\r\n", string.Empty);
            privateKeyPem = privateKeyPem.Replace("\r\n-----END RSA PRIVATE KEY-----\r\n", string.Empty);
            byte[] privateKeyRaw = Convert.FromBase64String(privateKeyPem);

            // Creating the RSA key, note the import function will only work with rsa keys. Otherwise it needs to be changed accordingly.
            RSACryptoServiceProvider provider = new RSACryptoServiceProvider();
            provider.ImportRSAPrivateKey(new ReadOnlySpan<byte>(privateKeyRaw), out _);
            RsaSecurityKey rsaSecurityKey = new RsaSecurityKey(provider);

            // Generating token
            var now = DateTime.UtcNow;

            var claims = new[]
            {
                    new Claim(JwtRegisteredClaimNames.Sub, userName),
                    new Claim(JwtRegisteredClaimNames.Azp, clientId),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var handler = new JwtSecurityTokenHandler();

            var token = new JwtSecurityToken(
                issuer: issuer,
                audience: clientId,
                claims: claims,
                notBefore: now.AddMilliseconds(-30),
                expires: now.AddMinutes(60),
                signingCredentials: new SigningCredentials(rsaSecurityKey, SecurityAlgorithms.RsaSha256));
            token.Header.Add("kid", kid);
            return handler.WriteToken(token);
        }
    }
}
