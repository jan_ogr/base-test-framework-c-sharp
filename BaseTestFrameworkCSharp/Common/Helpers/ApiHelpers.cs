﻿namespace BaseTestFrameworkCSharp.Common.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BaseTestFrameworkCSharp.App.Api;
    using NLog;

    public static class ApiHelpers
    {
        /// <summary>
        /// Static class for converting strings or httpresponsmessages to a T class instance.
        /// </summary>
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Generic cleanup method.
        /// </summary>
        /// <param name="api">IApi class.</param>
        /// <param name="deleteList">List with objects to be deleted.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task CleanUpAsync(this IApi api, List<long> deleteList)
        {
            foreach (var id in deleteList)
            {
                try
                {
                    var res = await api.GetById(id);
                    var clean = res.ToAsync<dynamic>().Result;
                    res = await api.Delete(clean.Id);
                }
                catch (Exception e)
                {
                    Logger.Warn(e.Message);
                }
            }

            deleteList.Clear();
        }

        /// <summary>
        /// Extenssion for updating the delete list after a post request.
        /// </summary>
        /// <typeparam name="T">Type T.</typeparam>
        /// <param name="response">HttpResponseMessage.</param>
        /// <param name="deleteList">List of dynamics with T type objects to be deleted.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public static async Task<HttpResponseMessage> AddToCleanUp<T>(this Task<HttpResponseMessage> response, List<T> deleteList)
                  where T : new()
        {
            if (response.Result.IsSuccessStatusCode)
            {
                var res = await response;
                deleteList.Add(res.ToAsync<T>().Result);
            }

            return await response;
        }
    }
}
