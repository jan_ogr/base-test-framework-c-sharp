﻿namespace BaseTestFrameworkCSharp.Common.RestClient
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using NLog;

    /// <summary>
    /// Retry handler for the Client class.
    /// </summary>
    public class RetryHandler : DelegatingHandler
    {
        private const int MaxRetries = 3;
        private static Logger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="RetryHandler"/> class.
        /// </summary>
        /// <param name="innerHandler">HttpMessageHandler instance.</param>
        public RetryHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
            logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// Overrides the send async method. If the response is not successfull there will be up to 3 retries executed.
        /// </summary>
        /// <param name="request">HttpRequestMessage.</param>
        /// <param name="cancellationToken">CancellationToken.</param>
        /// <returns>HttpRequestMessage instance.</returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;
            for (int i = 0; i < MaxRetries; i++)
            {
                try
                {
                    response = await base.SendAsync(request, cancellationToken);
                    if (response.IsSuccessStatusCode)
                    {
                        logger.Debug($"Request: {response.RequestMessage} \n Response: {response.StatusCode} \n");
                        return response;
                    }

                    logger.Warn($"Request: {response.RequestMessage} \n Response: {response.StatusCode} \n Body: {response.Content.ReadAsStringAsync().Result} \n");
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }
            }

            return response;
        }
    }
}
