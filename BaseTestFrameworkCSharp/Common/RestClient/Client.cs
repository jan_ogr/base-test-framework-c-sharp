﻿namespace BaseTestFrameworkCSharp.Common.RestClient
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Authentication;
    using System.Text;
    using System.Threading.Tasks;
    using BaseTestFrameworkCSharp.Common.Helpers;

    /// <summary>
    /// Gets or sets generic restclient class with default accept : application/json headers, baseurl and configured for ssl.
    /// Also implements the retryhandler.
    /// </summary>
    public class Client : IClient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Client"/> class.
        /// Client Constructor.
        /// </summary>
        /// <param name="baseUri">String base uri.</param>
        /// <param name="certificate">String path to certificate.</param>
        /// <param name="authenticationHeaders">AuthenticationHeaderValue.</param>
        public Client(Uri baseUri, string certificate = null, AuthenticationHeaderValue authenticationHeaders = null)
        {
            BaseUri = baseUri;
            Certificate = certificate;
            AuthenticationHeaders = authenticationHeaders;
        }

        public Uri BaseUri { get; set; }

        public AuthenticationHeaderValue AuthenticationHeaders { get; set; }

        public string Certificate { get; set; }

        /// <summary>
        /// Creates a new instance of HttpClient.
        /// </summary>
        /// <returns>HttpClient instance.</returns>
        public HttpClient GetClient()
        {
            var handler = new HttpClientHandler()
            {
                ClientCertificateOptions = ClientCertificateOption.Manual,
                SslProtocols = SslProtocols.Tls12,
            };

            var client = new HttpClient(new RetryHandler(handler))
            {
                BaseAddress = BaseUri,
            };
            client.DefaultRequestHeaders.Authorization = AuthenticationHeaders;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        /// <summary>
        /// Method fetching an oauth access token using the client credentials oauth flow.
        /// </summary>
        /// <param name="clientId">Client id used to fetch token.</param>
        /// <param name="clientSecret">Client secret used to fetch token.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<AuthenticationHeaderValue> GetAccessTokenAsync(string clientId, string clientSecret)
        {
            using HttpClient client = new HttpClient
            {
                BaseAddress = BaseUri,
            };

            client.DefaultRequestHeaders.Authorization = BasicAuthHeader(
                clientId, clientSecret);
            var res = await client.PostAsync("/oauth2/token", new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
            { new KeyValuePair<string, string>("grant_type", "client_credentials") }));
            var token = res.ToAsync<dynamic>().Result.accessToken;
            return new AuthenticationHeaderValue("Bearer", token);
        }

        /// <summary>
        /// Creates and returns a basic authentication header.
        /// </summary>
        /// <param name="userName">Basic auth name.</param>
        /// <param name="password">Basic auth password.</param>
        /// <returns>AuthenticationHeaderValue instance.</returns>
        public AuthenticationHeaderValue BasicAuthHeader(string userName, string password)
        {
            var byteArray = new UTF8Encoding().GetBytes($"{userName}:{password}");
            return new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
        }
    }
}
