﻿namespace BaseTestFrameworkCSharp.Common.RestClient
{
    using System;
    using System.Net.Http.Headers;

    /// <summary>
    /// Rest client interface, to be used as a contract for all rest client classes.
    /// </summary>
    public interface IClient
    {
        Uri BaseUri { get; set; }

        AuthenticationHeaderValue AuthenticationHeaders { get; set; }

        string Certificate { get; set; }
    }
}
