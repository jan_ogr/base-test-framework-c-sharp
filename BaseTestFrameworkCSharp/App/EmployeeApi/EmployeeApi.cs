﻿﻿namespace BaseTestFrameworkCSharp.App.EmployeeApi
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Base_test_framework_c_sharp.Models.Demo;
    using BaseTestFrameworkCSharp.App.Api;
    using BaseTestFrameworkCSharp.Common.Helpers;
    using BaseTestFrameworkCSharp.Common.RestClient;
    using BaseTestFrameworkCSharp.Models.Config;
    using Newtonsoft.Json;

    /// <summary>
    /// EmployApi class. Democlass on how to use the Client class.
    /// </summary>
    public class EmployeeApi : IApi
    {
        private static NLog.Logger logger;
        private readonly Config config;
        private readonly HttpClient httpClient;
        private readonly List<CreatedEmployee> deleteList;
        private UrlHelpers urlHelpers;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmployeeApi"/> class.
        /// Constructor.
        /// </summary>
        /// <param name="conf">Config class instance.</param>
        public EmployeeApi(Config conf)
        {
            config = conf;
            urlHelpers = new UrlHelpers(config);
            httpClient = new Client(urlHelpers.UrlBuilder(false, new List<string> { config.Rest.Version })).GetClient();

            // Needed for the dummyservice
            httpClient.DefaultRequestHeaders.Add("User-Agent", "XY");
            logger = NLog.LogManager.GetCurrentClassLogger();
            deleteList = new List<CreatedEmployee>();
        }

        /// <summary>
        /// Get method fetching multiple employees.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<HttpResponseMessage> Get()
        {
            return await httpClient.GetAsync("employees");
        }

        /// <summary>
        /// Get method fetching a specific employee using id.
        /// </summary>
        /// <param name="id">Employee id, long.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<HttpResponseMessage> GetById(long id)
        {
            return await httpClient.GetAsync($"employee/{id}");
        }

        /// <summary>
        /// POST method for creating a new employee instance.
        /// </summary>
        /// <param name="employee">A new instance of the employee class.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<HttpResponseMessage> Create(dynamic employee)
        {
            return await httpClient.PostAsync("create", new StringContent(JsonConvert.SerializeObject(employee), Encoding.UTF8, "application/json")).AddToCleanUp(deleteList);
        }

        /// <summary>
        /// Put Method for updating an existing employee.
        /// </summary>
        /// <param name="id">Employee id, long.</param>
        /// <param name="employee">The user to be updated.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<HttpResponseMessage> Update(long id, dynamic employee)
        {
            return await httpClient.PutAsync($"update/{id}", new StringContent(JsonConvert.SerializeObject(employee), Encoding.UTF8, "application/json"));
        }

        /// <summary>
        /// Delete method for removing existing employees.
        /// </summary>
        /// <param name="id">Employee id, long.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task<HttpResponseMessage> Delete(long id)
        {
            return await httpClient.DeleteAsync($"delete/{id}");
        }

        /// <summary>
        /// Polls for a specific user 20 times and waits 1 second inbetween every try.
        /// </summary>
        /// <param name="id">user id.</param>
        /// <param name="expectedStatusCode">expected HttpStatusCode.</param>
        /// <returns>Task HttpResponseMessage instance.</HttpResponseMessage></HttpResponseMessageZ></returns>
        public async Task<HttpResponseMessage> Poll(long id, HttpStatusCode expectedStatusCode)
        {
            HttpResponseMessage res = null;
            for (int i = 0; i < 20; i++)
            {
                res = await httpClient.GetAsync($"employees/{id}");
                if (res.StatusCode == expectedStatusCode)
                {
                    return res;
                }

                Thread.Sleep(1000);
            }

            return res;
        }

        /// <summary>
        /// Cleanup method for the EmployeeApi class.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        public async Task CleanUpAsync()
        {
            foreach (var employee in deleteList)
            {
                try
                {
                    await Delete(employee.Data.Id);
                }
                catch
                {
                    logger.Error($"Failed to delete user with id {employee.Data.Id}");
                }
            }

            deleteList.Clear();
        }
    }
}
