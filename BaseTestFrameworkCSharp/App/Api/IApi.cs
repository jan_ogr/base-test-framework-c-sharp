﻿namespace BaseTestFrameworkCSharp.App.Api
{
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// Public interface for your api classes.
    /// </summary>
    public interface IApi
    {
        Task<HttpResponseMessage> Get();

        Task<HttpResponseMessage> Delete(long id);

        Task<HttpResponseMessage> GetById(long id);

        Task<HttpResponseMessage> Update(long id, dynamic data);

        Task<HttpResponseMessage> Create(dynamic data);

        Task CleanUpAsync();
    }
}
