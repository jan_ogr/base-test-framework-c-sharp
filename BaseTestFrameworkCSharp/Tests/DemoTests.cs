﻿namespace BaseTestFrameworkCSharp.Tests
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using Base_test_framework_c_sharp.Models.Demo;
    using BaseTestFrameworkCSharp.App.EmployeeApi;
    using BaseTestFrameworkCSharp.Common.Helpers;
    using BaseTestFrameworkCSharp.Models.Config;
    using BaseTestFrameworkCSharp.Models.Demo;
    using FluentAssertions;
    using NLog;
    using NUnit.Framework;

    /// <summary>
    /// Demo test class.
    /// </summary>
    public class DemoTests : BaseTest
    {
        private Config conf;
        private EmployeeApi employeeApi;
        private Logger logger;

        /// <summary>
        /// One time setup, executed once per test class.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            conf = ConfigHelper.GetConfig();
            employeeApi = new EmployeeApi(conf);
            logger = LogManager.GetCurrentClassLogger();
        }

        /// <summary>
        /// One time tear down, executed at the end of every test class.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        [OneTimeTearDown]
        public async Task TearDownAsync()
        {
            await employeeApi.CleanUpAsync();
        }

        /// <summary>
        /// Test case for fetching employees.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetEmployeesAsync()
        {
            var res = await employeeApi.Get();
            Assert.AreEqual(HttpStatusCode.OK, res.StatusCode);
        }

        /// <summary>
        /// Test case for fetching employeess and validating the response code using fluent assertions.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetEmployeesAsyncFluentAssertions()
        {
            var res = await employeeApi.Get();
            res.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        /// <summary>
        /// Demo test creating and verifying an employee.
        /// </summary>
        /// <param name="age">Age in string format.</param>
        /// <param name="name">Employee name, string.</param>
        /// <param name="salary">Employee salary, string.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("30", "Bruce Lee", "Billion billion", TestName = "CreateNewEmployee")]
        public async Task CreateEmployee(string age, string name, string salary)
        {
            // Create employee
            var res = await employeeApi.Create(new CreateEmployee { Age = age, Id = Guid.NewGuid().ToString(), Name = name, Salary = salary });
            res.StatusCode.Should().Be(HttpStatusCode.OK);

            // Verify employee attributes are correct
            var employee = await res.ToAsync<CreatedEmployee>();
            employee.Data.Age.Should().Be(age);
            employee.Data.Name.Should().Be(name);
            employee.Data.Salary.Should().Be(salary);
        }

        /// <summary>
        /// Demo test creating and deleting an employee.
        /// </summary>
        /// <param name="age">Age in string format.</param>
        /// <param name="name">Employee name, string.</param>
        /// <param name="salary">Employee salary, string.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("30", "Bruce Lee", "Billion billion", TestName = "CreateAndDeleteEmployee")]
        public async Task CreateAndDeleteEmployee(string age, string name, dynamic salary)
        {
            // Create employee
            var res = await employeeApi.Create(new CreateEmployee { Age = age, Id = Guid.NewGuid().ToString(), Name = name, Salary = salary });
            res.StatusCode.Should().Be(HttpStatusCode.OK);

            // Verify employee attributes are correct
            var employee = await res.ToAsync<CreatedEmployee>();
            employee.Data.Age.Should().Be(age);
            employee.Data.Name.Should().Be(name);
            employee.Data.Salary.ToString().Should().Be($"{salary}");

            // Delete employee
            res = await employeeApi.Delete(employee.Data.Id);
            res.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        /// <summary>
        /// Demo test creating and updating an employee.
        /// </summary>
        /// <param name="age">Age in string format.</param>
        /// <param name="name">Employee name, string.</param>
        /// <param name="salary">Employee salary, string.</param>
        /// <param name="newSalary">Employee new salary, string.</param>
        /// <param name="newName">Employee new name, string.</param>
        /// <param name="expectedStatusCode">Expected response code, HttpStatusCode.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("30", "Bruce Lee", "Billion billion", "trillion billion", "Hasse Andersson", HttpStatusCode.OK, TestName = "CreateAndUpdateEmployee")]
        [TestCase("30", "Bruce Lee", "Billion billion", new int[] { 1, 2, 3 }, new int[] { 1, 2, 3 }, HttpStatusCode.BadRequest, TestName = "CreateAndUpdateEmployeeUsingIncorrectValuesReturnsBadRequest", Ignore = "Incorrect behaviour")]
        public async Task CreateAndUpdateEmployee(string age, string name, string salary, dynamic newSalary, dynamic newName, HttpStatusCode expectedStatusCode)
        {
            // Create employee
            var res = await employeeApi.Create(new CreateEmployee { Age = age, Id = Guid.NewGuid().ToString(), Name = name, Salary = salary });
            res.StatusCode.Should().Be(HttpStatusCode.OK);

            // Verify employee attributes are correct
            var employee = await res.ToAsync<CreatedEmployee>();
            employee.Data.Age.Should().Be(age);
            employee.Data.Name.Should().Be(name);
            employee.Data.Salary.Should().Be(salary);

            // Update employee
            res = await employeeApi.Update(employee.Data.Id, new { Age = age, Id = employee.Data.Id, Name = newName, Salary = newSalary });
            res.StatusCode.Should().Be(expectedStatusCode);

            if (expectedStatusCode == HttpStatusCode.OK)
            {
                // Verify updates
                employee = await res.ToAsync<CreatedEmployee>();
                employee.Data.Name.Should().Be(newName);
                employee.Data.Salary.Should().Be(newSalary);
            }
        }

        /// <summary>
        /// Gets an employee by id.
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        public async Task GetEmployeeById()
        {
            var res = await employeeApi.Get();
            res.StatusCode.Should().Be(HttpStatusCode.OK);
            var employees = res.ToAsync<Employees>().Result;

            res = await employeeApi.GetById(employees.Data.FirstOrDefault().Id);
            res.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }
}
