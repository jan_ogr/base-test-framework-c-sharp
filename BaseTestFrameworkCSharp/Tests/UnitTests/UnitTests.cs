﻿namespace Base_test_framework_c_sharp.Tests.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using BaseTestFrameworkCSharp.Common.Helpers;
    using BaseTestFrameworkCSharp.Models.Config;
    using BaseTestFrameworkCSharp.Models.Demo;
    using FluentAssertions;
    using Newtonsoft.Json;
    using NUnit.Framework;

    public class UnitTests : BaseTest
    {
        private Config conf;
        private UrlHelpers urlHelpers;

        /// <summary>
        /// One time setup, executed once per test class.
        /// </summary>
        [OneTimeSetUp]
        public void Setup()
        {
            conf = ConfigHelper.GetConfig();
            urlHelpers = new UrlHelpers(conf);
        }

        /// <summary>
        /// Unit test for the add parameters method.
        /// </summary>
        [Test]
        public void TestUrlParameters()
        {
            var baseUrl = "https://www.google.com";
            var parameters = new Dictionary<string, string>() { { "a", "1" }, { "b", "2" } };
            var url = UrlHelpers.AddParameters(baseUrl, parameters);
            Assert.AreEqual($"{baseUrl}?a=1&b=2", url);
        }

        /// <summary>
        /// Data driven test method verifying the url builder method.
        /// </summary>
        /// <param name="ssl">Bool value for turning ssl on or off.</param>
        /// <param name="url">Correct url string for the assert.</param>
        [Test]
        [TestCase(true, "https://dummy.restapiexample.com/api/test/url/lolol/foiegras/", TestName = "TestUrlBuilderHttps")]
        [TestCase(false, "http://dummy.restapiexample.com/api/test/url/lolol/foiegras/", TestName = "TestUrlBuilderHttp")]
        public void TestUrlBuilder(bool ssl, string url)
        {
            var relativePath = new List<string> { "test", "url", "lolol", "foiegras" };
            Assert.AreEqual(new Uri(url), urlHelpers.UrlBuilder(ssl, pathList: relativePath));
        }

        /// <summary>
        /// Test case verifying the ToAsync Method.
        /// </summary>
        /// <param name="address">test data, address.</param>
        /// <param name="name">Test data, name.</param>
        /// <returns>A <see cref="Task"/> representing the asynchronous unit test.</returns>
        [Test]
        [TestCase("Ankeborg 12345", "Kalle Anka", TestName = "VerifyToAsyncMethod")]
        public async Task VerifyToAsyncMethod(string address, string name)
        {
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(new User { Address = address, Id = Guid.NewGuid(), Name = name })),
            };
            var user = await response.ToAsync<User>();
            user.Name.Should().Be(name);
            user.Address.Should().Be(address);
        }

        /// <summary>
        /// Test case verifying the To Method.
        /// </summary>
        /// <param name="address">test data, address.</param>
        /// <param name="name">Test data, name.</param>
        [Test]
        [TestCase("Ankeborg 12345", "Kalle Anka", TestName = "VerifyToMethod")]
        public void VerifyToMethod(string address, string name)
        {
            var content = JsonConvert.SerializeObject(new User { Address = address, Id = Guid.NewGuid(), Name = name });
            var user = content.To<User>();
            user.Name.Should().Be(name);
            user.Address.Should().Be(address);
        }

        /// <summary>
        /// Test case verifying the config object is of type Config.
        /// </summary>
        [Test]
        public void VerifyConfigHelper()
        {
            var config = ConfigHelper.GetConfig();
            config.Should().BeOfType<Config>();
        }
    }
}
