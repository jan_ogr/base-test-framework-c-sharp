pipeline {
    agent any

	options {
    //Discard old builds.
		buildDiscarder(logRotator(numToKeepStr: '30', artifactNumToKeepStr: '20'))
	}

    environment {
		API_KEY = credentials('API_KEY')
		MONGODB_PSW = credentials('MONGODB_PSW')
		SERVER = credentials('SERVER')
		USER = credentials('USER')
    }

    stages {
        stage('Build') {
            steps {
                bat 'dotnet restore'
                bat 'dotnet build'
                bat 'dotnet publish'
                bat 'doxygen doxygenfile'
            }
        }
		stage('Run-tests') {
			steps {
			    bat 'dotnet test --logger "trx;LogFileName=TestResults.trx"'
			}
		}
    }

	// Post build actions, e.g archiving
	 post {
        always {
			mstest testResultsFile:"BaseTestFrameworkCSharp/TestResults/TestResults.trx", keepLongStdio: true
			dir('ImportScript'){
				bat 'python import_data.py %BUILD_NUMBER% %USER% %API_KEY% %MONGODB_PSW% %SERVER%'
			}
            archiveArtifacts artifacts: '**/*.*', fingerprint: true
            script {
                def htmlFiles
                dir ('html') {
                    htmlFiles = findFiles glob: '*.html'
                }
                publishHTML([
                        reportDir: 'html',
                        reportFiles: 'index.html',
                        reportName: 'Test code documentation',
                        allowMissing: false,
                        alwaysLinkToLastBuild: true,
                        keepAll: true])
            }
            
            deleteDir()
        }
    }
}